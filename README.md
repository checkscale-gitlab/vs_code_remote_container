vs_code_remote_container
------------------------

__Instructions for using VS Code Remote Explorer for a container__


These instructions assume you have a computer running the macOS, Linux, and Windows 10 operating systems and have previously installed:

* Docker Desktop. Download and install Docker Desktop for your target development environment, https://www.docker.com/products/docker-desktop
* Visual Studio Code, [Download and install it](https://code.visualstudio.com/download) and the [Visual Studio Code Remote Development Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack).

Install Remote - Containers
---------------------------

Select the 'Extensions' sidebar menu item and enter `remote` in the Search Extensions in Marketplace dialog box and install this extension (ms-vscode-remote.remote-containers).

You can also locate and install the [Visual Studio Code Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) from the web link.

> Note: You may need to restart VS code after installing the Extension Pack for the extension to be recognized.

Create a directory and workspace
--------------------------------

Create a local directory, or create a new remote repository on GitHub / GitLab and clone it to your local machine.

Select Code -> File -> Save Workspace As

Add the current folder to the workspace.

Create User and Workspace settings
----------------------------------

Select the Gear Icon on the menu (bottom) and Select Settings (https://code.visualstudio.com/docs/getstarted/settings)

You can modify the User, Workspace and Folder settings.

Settings have precedence: (from most to least)

Workspace Folder
Workspace setting
User settings

If you modify the workspace settings, they will be saved in your workspace.code-workspace file

Create the `.devcontainer` folder
---------------------------------

Create (or save a copy from this repository) the `.devcontainer` folder and `devcontainer.json` files.  You may wish to change the name value in the configuration file and add or update other values. Specify the name of your Dockerfile. The Dockerfile can be in the `.devcontainer` directory. However, putting the `Dockerfile` in the root of the folder allows your to copy the requirements file(s).

Create the Dockerfile
---------------------

Copy or save the Dockerfile into your folder. The sample `Dockerfile` creates a python virtual environment in the container and installs Ansible in the virtual environment. It also upgrades `pip`, installs the python packages in `requirements.txt` and uses `ansible-galaxy` to install any necessary collections or roles.

Examine and modify the respective requirement file(s). Modify the name of the Python venv by updating the `ENV VIRTUAL_ENV` line.

(Re)Build and Open the container
--------------------------------

Select the Gear Icon and select the Command Palette (or click the `><`) icon in the lower left and type in `remote` in the dialog box,. Select `Remote-Container: Rebuild and Reopen in Container`  Select the `show log` in the lower right corner to observe the steps of building and running the container.

If the build is successful, open a new terminal window. You should should see a prompt similar to:

```shell
root@ff9c3cca27f2:/workspaces/vs_code_remote_container
```

Issue a `ls -salt` command in the prompt, all the files in your local folder should appear. In the lower left corner of VS Code, you should see the name you entered in the `.devcontainer/devcontainer.json` configuration file.

From the terminal prompt, issue `touch CREATEDINCONTAINER` and note the file appears in the Explorer pane. The file created and edited in the container appears on your local system and will exist after the container is stopped or destroyed.

Open a new terminal. From the terminal prompt, issue `ansible --version`. 

```shell
root@ff9c3cca27f2:/workspaces/vs_code_remote_container# ansible --version
ansible 2.10.9
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/ansible210/lib/python3.8/site-packages/ansible
  executable location = /opt/ansible210/bin/ansible
  python version = 3.8.10 (default, May 12 2021, 15:56:47) [GCC 8.3.0]
```

>Note: the Ansible Python module is the Python virtual environment and the Ansible executable is within the virtual environment as well.

The Ansible collection(s) by default are installed in `/root/.ansible/`.

Docker Desktop
--------------

Open your Docker Deskop window and examine the Docker image and container which has been created.

Close the Remote Connection
---------------------------

When finished, you can close the remote connection by clicking the `><` icon and selecting that option.

Re-open the Remote Container
----------------------------

Select the Command Palette and `Open Workspace in Remote Container`. The container will be restarted and accessable again. 

Author
------

joel.king@wwt.com @joelwking
